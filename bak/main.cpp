#include <Arduino.h>
#include <avr/wdt.h>

#define F_CPU 11059000  //Mhz cristal

#define TRANSISTOR_PIN  10
#define COOLER_PIN      11
#define PRESCALER   3   // (11.059mhz/64)=172.8khz, 5.78us de resolucion en el timer, 380ms maximo.
#define POS_SENSOR 21  // grados de adelanto en el sensor de cigueñal.
#define DWELL 10000     // Dwell en microsegundos (6ms -> RPM maxima= 10.000)
#define RPM_MIN 500   // RPM donde comienza a funcionar el avance
#define RPM_MAX 6000    // RPM para el avance máximo
#define AVAN_MIN 8      // avance inicial
#define AVAN_MAX 19     // Avance maximo (siempre menor que la posicion del sensor)
#define MIN_SK_TIME 600 // Tiempo minimo de chispa en microsegundos
#define US_FOR_TICK   (64000000.0/F_CPU)

volatile enum f_pulse { FALSE,TRUE } first_pulse;
volatile uint16_t periodo;
volatile float rpm,grados;
void setup()
{
  // Configuro el timer 1
  TCCR1A = 0;             // todos los valores por defecto.
  TCCR1B = PRESCALER;     //plancho los bits del prescaler, el resto en cero (por defecto)
  TIMSK1 |= (1 << TOIE1); // Habilito la interrupción del overflow
  TIMSK1 |= (1<<OCIE1B) | (1<<OCIE1A); // Habilito las interrupciones para generar el pulso

  // Configuro la interrupcion externa
  EICRA |= (1 << ISC01) | (1 << ISC00); //en flanco positivo
  EIMSK |= (1<<INT0);// habilito la interrupcion del pin.


  pinMode(TRANSISTOR_PIN, OUTPUT);
  digitalWrite(TRANSISTOR_PIN, HIGH);
  pinMode(COOLER_PIN, OUTPUT); //prendo el cooler
  pinMode(7, OUTPUT);
  digitalWrite(7, HIGH); //Enciendo el relé.
  sei();
  Serial.begin(9600*16.0/11.059);
  //wdt_enable(WDTO_1S);
}

// Arduino usa el Timer0 para millis, mejor descativar esa interrupcion si no se va a usar.
void grados_avance(void)
{ //Retorna los microsegundos de retardo para lograr el avance deseado
  rpm = 60000000/(periodo*US_FOR_TICK); // 60 segundos para calcular RPM
  if (rpm < RPM_MIN){
    grados=POS_SENSOR;
    return ; // avance nulo (chispa en el TPS para facilitar el arranque)
  }
    
  if (rpm > RPM_MAX){
    grados=(POS_SENSOR - AVAN_MAX); 
    return; // si voy a mas de 4k uso el avance máximo.
  }
  // si no caí en ninguno de los anteriores calculo el avance proporcional
  grados = POS_SENSOR-(((rpm - RPM_MIN)*(AVAN_MAX-AVAN_MIN)) / (RPM_MAX - RPM_MIN) + AVAN_MIN); // El avance en grados por 1000
  return;
}

void loop()
{
    Serial.print(rpm);//RPM
    Serial.print('\t');
    Serial.print(POS_SENSOR-grados);//grados
    Serial.print('\t');
    Serial.print((grados*periodo)/360);
    Serial.print('\t');
    Serial.print(OCR1B);
    Serial.print('\t');
    Serial.println(periodo);
    delay(100);
    if(rpm>3500) analogWrite(COOLER_PIN,200);
    else analogWrite(COOLER_PIN,100);
    
  // put your main code here, to run repeatedly:
  //wdt_reset();
}



ISR(INT0_vect)
{ periodo = TCNT1;
  TCNT1 = 0;//reseteo el contador para que arranque de cero.
  TCCR1B |= PRESCALER; // seteo el prescaler (habilito el timer)
  if (first_pulse == TRUE){
    first_pulse = FALSE;
    return; // si es el primer pulso desde que se apago  el motor lo ignoro
  }
  OCR1A = periodo*grados/360; // grados de atraso respecto del sensor.
  if(periodo*US_FOR_TICK>DWELL+MIN_SK_TIME){ //me fijo si hay tiempo para tanto Dwell, en caso que no, fijo el tiempo de chispa en 600us
  OCR1B = periodo - DWELL / US_FOR_TICK;}  // valor de Dwell, programo una interrupcion para comenzar el Dwell
  else{
  OCR1B = OCR1A+MIN_SK_TIME/US_FOR_TICK;  }
  grados_avance();
}

ISR(TIMER1_COMPA_vect)
{                                    // TODO: quizas convenga hacer una interrupcion NAKED
 EIMSK &= ~(1 << INT0); // Deshabilito la interrupción externa para evitar ruido en la chispa
  digitalWrite(TRANSISTOR_PIN,HIGH); //Disparo la chispa
 _delay_us(100); //tiempo muerto (para evitar errores)
 EIFR |= (1 << INTF0); //borro el flag escribiendo un 1
 EIMSK |= (1<<INT0);// habilito la interrupcion del pin. por que termino la chispa (evito el ruido)

}

ISR(TIMER1_COMPB_vect)
{
  //pinMode(2,INPUT_PULLUP);// pongo el pin de la interrupcon como entrada cuando el ruido pasó.
  //_delay_us(50);
  digitalWrite(TRANSISTOR_PIN,LOW); // Comienzo el tiempo de Dwell
 }

ISR(TIMER1_OVF_vect)
{
  digitalWrite(TRANSISTOR_PIN,LOW); // Si desborda el timer y no llego ninguna interrupcion descargo la chispa
  first_pulse = TRUE;
  TCCR1B &= ~PRESCALER; // deshabilito el clock al timer
  TCNT1 = 65535;
  digitalWrite(TRANSISTOR_PIN, HIGH);
  rpm=0;
  periodo=65535;
}

ISR(BADISR_vect)
{
 // digitalWrite(TRANSISTOR_PIN, LOW);
}
